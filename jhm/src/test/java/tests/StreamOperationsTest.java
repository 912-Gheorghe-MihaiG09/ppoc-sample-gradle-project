package tests;

import org.example.StreamOperations;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StreamOperationsTest {

    @Test
    public void testSumOfElementsDouble() {
        List<Double> numbers = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0);
        assertEquals(15.0, StreamOperations.sumOfElementsDouble(numbers), 0.0001);
    }

    @Test
    public void testAverageOfElementsDouble() {
        List<Double> numbers = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0);
        assertEquals(3.0, StreamOperations.averageOfElementsDouble(numbers), 0.0001);
    }

    @Test
    public void testTop10PercentDouble() {
        List<Double> numbers = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0);
        List<Double> top10Percent = StreamOperations.top10PercentDouble(numbers);

        // The top 10% of the list is [15.0, 14.0]
        assertEquals(Arrays.asList(15.0, 14.0), top10Percent);
    }

    @Test
    public void testSumOfElementsPrimitive() {
        List<Double> numbers = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0);
        assertEquals(15.0, StreamOperations.sumOfElementsPrimitive(numbers), 0.0001);
    }

    @Test
    public void testAverageOfElementsPrimitive() {
        List<Double> numbers = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0);
        assertEquals(3.0, StreamOperations.averageOfElementsPrimitive(numbers), 0.0001);
    }

    @Test
    public void testTop10PercentPrimitive() {
        List<Double> numbers = Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0);
        List<Double> top10Percent = StreamOperations.top10PercentPrimitive(numbers);

        // The top 10% of the list is [15.0, 14.0]
        assertEquals(Arrays.asList(15.0, 14.0), top10Percent);
    }
}