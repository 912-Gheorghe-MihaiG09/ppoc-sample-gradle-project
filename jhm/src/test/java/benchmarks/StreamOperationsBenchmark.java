package benchmarks;
import org.example.StreamOperations;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Arrays;
import java.util.List;

@Fork(1)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 2, time = 1)
@State(Scope.Thread)
public class StreamOperationsBenchmark {
    @Param({"10000", "100000", "1000000"})
    public int size;

    List<Double> numbers;

    @Setup
    public void setup() {
        numbers = generateRandomNumbers(size);
    }

    @Benchmark
    public void sumOfElementsDouble(Blackhole consumer) {
        consumer.consume(StreamOperations.sumOfElementsDouble(numbers));
    }

    @Benchmark
    public void averageOfElementsDouble(Blackhole consumer) {
        consumer.consume(StreamOperations.averageOfElementsDouble(numbers));
    }

    @Benchmark
    public void top10PercentDouble(Blackhole consumer) {
        consumer.consume(StreamOperations.top10PercentDouble(numbers));
    }
    @Benchmark
    public void sumOfElementsPrimitive(Blackhole consumer) {
        consumer.consume(StreamOperations.sumOfElementsPrimitive(numbers));
    }

    @Benchmark
    public void averageOfElementsPrimitive(Blackhole consumer) {
        consumer.consume(StreamOperations.averageOfElementsPrimitive(numbers));
    }

    @Benchmark
    public void top10PercentPrimitive(Blackhole consumer) {
        consumer.consume(StreamOperations.top10PercentPrimitive(numbers));
    }

    // Helper method to generate random numbers
    private static List<Double> generateRandomNumbers(int size) {
        Double[] array = new Double[size];
        for (int i = 0; i < size; i++) {
            array[i] = Math.random();
        }
        return Arrays.asList(array);
    }

    public static void main(String[] args) throws org.openjdk.jmh.runner.RunnerException {
        Options opt = new OptionsBuilder()
                .include(StreamOperationsBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new org.openjdk.jmh.runner.Runner(opt).run();
    }
}
