package benchmarks;

import org.example.repository.ArrayListBasedRepository;
import org.example.repository.HashSetBasedRepository;
import org.example.repository.InMemoryRepository;
import org.example.repository.TreeSetBasedRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Objects;

@Fork(1)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 2, time = 1)
@Measurement(iterations = 2, time = 1)
@State(Scope.Thread)
public class Benchmarks {
    @Param({"5", "10", "20"})
    public int size;

    @Param({"hashSet"})
    public String repoType;

    InMemoryRepository<Integer> repo;

    @Setup
    public void setup() {
        if (Objects.equals(repoType, "hashSet")) {
            repo = new HashSetBasedRepository<>();
        } else if (Objects.equals(repoType, "arrayList")){
            repo = new ArrayListBasedRepository<>();
        } else if (Objects.equals(repoType, "treeSet")) {
            repo = new TreeSetBasedRepository<>();
        }

        for (int i = 0; i < size; i++) {
            repo.add(i);
        }
    }

    @Benchmark
    public void add(Blackhole consumer) {
        for (int i = 0; i < size; i++) {
            repo.add(i);
        }
    }

    @Benchmark
    public void remove(Blackhole consumer) {
        for (int i = 0; i < size; i++) {
            repo.remove(i);
        }
    }

    @Benchmark
    public void contains(Blackhole consumer) {
        for (int i = 0; i < size; i++) {
            consumer.consume(repo.contains(i));
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(Benchmarks.class.getSimpleName())
//                .addProfiler(HotspotMemoryProfiler.class)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
