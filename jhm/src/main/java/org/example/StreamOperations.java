package org.example;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class StreamOperations {

    public static Double sumOfElementsDouble(List<Double> numbers) {
        return numbers.stream()
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    public static Double averageOfElementsDouble(List<Double> numbers) {
        return numbers.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0);
    }

    public static List<Double> top10PercentDouble(List<Double> numbers) {
        int size = numbers.size();
        int n = (int) Math.ceil(size * 0.1);

        return numbers.stream()
                .sorted((a, b) -> Double.compare(b, a))
                .limit(n)
                .collect(Collectors.toList());
    }

    public static double sumOfElementsPrimitive(List<Double> numbers) {
        return numbers.stream()
                .mapToDouble(Double::doubleValue)
                .sum();
    }

    public static double averageOfElementsPrimitive(List<Double> numbers) {
        return numbers.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0.0);
    }

    public static List<Double> top10PercentPrimitive(List<Double> numbers) {
        int size = numbers.size();
        int n = (int) Math.ceil(size * 0.1);

        return numbers.stream()
                .sorted((a, b) -> Double.compare(b, a))
                .limit(n)
                .collect(Collectors.toList());
    }
}