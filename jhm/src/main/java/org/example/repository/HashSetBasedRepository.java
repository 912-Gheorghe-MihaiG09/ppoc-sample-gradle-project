package org.example.repository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements  InMemoryRepository<T>{
    Set<T> set;

    public HashSetBasedRepository() {
        this.set = new HashSet<T>();
    }

    @Override
    public void add(T object) {
    }

    @Override
    public boolean contains(T object) {
        return false;
    }

    @Override
    public void remove(T object) {

    }
}
