package org.example.repository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Comparable<T>> implements  InMemoryRepository<T>{
    Set<T> set;

    public TreeSetBasedRepository() {
        this.set = new TreeSet<T>();
    }

    @Override
    public void add(T object) {
        this.set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return this.set.contains(object);
    }

    @Override
    public void remove(T object) {
        this.set.remove(object);
    }
}
