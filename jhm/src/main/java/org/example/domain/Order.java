package org.example.domain;

import java.util.Objects;

public class Order implements Comparable<Order> {
    int id;
    int price;
    int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id && price == order.price && quantity == order.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price, quantity);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    int CompareTo(Order other){
        if(this.price * this.quantity > other.price * other.quantity)
            return 1;
            else if(this.price * this.quantity == other.price * other.quantity)
                return 0;
            return -1;
    }

    @Override
    public int compareTo(Order other) {
        if(this.price * this.quantity > other.price * other.quantity)
            return 1;
        else if(this.price * this.quantity == other.price * other.quantity)
            return 0;
        return -1;
    }
}
