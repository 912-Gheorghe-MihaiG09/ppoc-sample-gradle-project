package org.example;

public class Calculator {
    private Integer currentValue = 0;

    public void add(Integer value){
        this.currentValue += value;
    }
    public void subtract(Integer value){
        this.currentValue -= value;
    }

    public Integer display(){
        return this.currentValue;
    }

}
